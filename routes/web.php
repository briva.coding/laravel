<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\Auth\RegisterController;

Route::get('/', 'HomeController@Home');
Route::get('/home', 'HomeController@Home');

Route::get('/register','RegisterController@Register');

Route::get('/welcome','WelcomeController@Welcome');

Route::post('/welcome','WelcomeController@Welcome');


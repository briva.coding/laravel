<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class WelcomeController extends Controller
{
    public function Welcome(Request $request){
        // dd($request['Firstnama']);
        // dd($request->all());
        $NamaDepan =$request['Firstnama'];
        $NamaBelakang=$request['Lastnama'];

        return view('welcome', compact('NamaDepan', 'NamaBelakang'));
    }
}
